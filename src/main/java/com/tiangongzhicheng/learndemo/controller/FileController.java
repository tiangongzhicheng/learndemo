package com.tiangongzhicheng.learndemo.controller;


import com.tiangongzhicheng.learndemo.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/file")
public class FileController {

    @Autowired
    private FileService fileService;

    @RequestMapping("downFileFromUrl")
    public void downFileFromUrl(String url){
        fileService.downFileFromUrl(url);
    }
}
