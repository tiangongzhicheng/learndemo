package com.tiangongzhicheng.learndemo.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/provider")
public class ProviderController {

    @RequestMapping("/testProvider")
    public String testProvider(String param){
        return "这是为你提供的服务";
    }

}
