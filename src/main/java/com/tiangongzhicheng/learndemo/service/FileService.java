package com.tiangongzhicheng.learndemo.service;

import com.tiangongzhicheng.learndemo.utils.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class FileService {

    @Autowired
    private FileUtils fileUtils;

    public void downFileFromUrl(String url){
        try {
            fileUtils.downloadFile("aaa",url);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
