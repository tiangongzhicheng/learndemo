package com.tiangongzhicheng.learndemo.exception;

public class ExcelException extends RuntimeException {
    public ExcelException(String message) {
        super(message);
    }
}