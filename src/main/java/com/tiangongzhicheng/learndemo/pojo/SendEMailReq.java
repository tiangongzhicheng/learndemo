package com.tiangongzhicheng.learndemo.pojo;

import lombok.Data;

import java.util.List;


@Data
public class SendEMailReq {

    private String from;
    private String password;
    private String to;
    private String subject;
    private String content;
    private Integer type;
    private List<String> cc;
    private List<FileList> fileLists;

}
