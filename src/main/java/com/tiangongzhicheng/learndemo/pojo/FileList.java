package com.tiangongzhicheng.learndemo.pojo;

import lombok.Data;

@Data
public class FileList {
    private String fileUrl;
    private String fileName;
}
