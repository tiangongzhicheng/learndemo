package com.tiangongzhicheng.learndemo.utils;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerFontProvider;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.html.CssAppliers;
import com.itextpdf.tool.xml.html.CssAppliersImpl;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;
import lombok.Cleanup;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.icepdf.core.pobjects.Document;
import org.icepdf.core.pobjects.Page;
import org.icepdf.core.util.GraphicsRenderingHints;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.Iterator;


public class PdfUtil {

    /**
     *
     *
     * @param inputFile  需要生成缩略图的书籍的完整路径
     * @param outputFile 生成缩略图的放置路径
     */
    public static void generateBookImage(String inputFile, InputStream inputStream, String outputFile) {
        Document document = new Document();;

        try {
            float rotation = 0f;
            //缩略图显示倍数，1表示不缩放，0.5表示缩小到50%
            float zoom = 1f;

//            document =
            if (!StringUtils.isEmpty(inputFile)) {

                document.setFile(inputFile);
            } else if (null != inputStream) {
                document.setInputStream(inputStream, null);
            }
            // maxPages = document.getPageTree().getNumberOfPages();
            BufferedImage image = (BufferedImage) document.getPageImage(0, GraphicsRenderingHints.SCREEN,
                    Page.BOUNDARY_CROPBOX, rotation, zoom);

            Iterator iter = ImageIO.getImageWritersBySuffix("jpg");
            ImageWriter writer = (ImageWriter) iter.next();
            @Cleanup
            FileOutputStream out = new FileOutputStream(new File(outputFile));
            @Cleanup
            ImageOutputStream outImage = ImageIO.createImageOutputStream(out);

            writer.setOutput(outImage);
            writer.write(new IIOImage(image, null, null));

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            document.dispose();
        }
    }


    /**
     * 从网络Url中下载文件
     *
     * @param urlStr
     * @param fileName
     * @param savePath
     * @throws IOException
     */
    public static String downLoadFromUrl(String urlStr, String fileName, String savePath) throws IOException {
        URL url = new URL(urlStr);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        //设置超时间为3秒
        conn.setConnectTimeout(3 * 1000);
        //防止屏蔽程序抓取而返回403错误
        conn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");

        //得到输入流
        InputStream inputStream = conn.getInputStream();
        //获取自己数组
        byte[] getData = readInputStream(inputStream);

        //文件保存位置
        File saveDir = new File(savePath);
        if (!saveDir.exists()) {
            saveDir.mkdir();
        }
        File file = new File(saveDir + File.separator + fileName);
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(getData);
        if (fos != null) {
            fos.close();
        }
        if (inputStream != null) {
            inputStream.close();
        }


        System.out.println("info:" + savePath + " download success");
        return file.getAbsolutePath();
    }

    /**
     * 从输入流中获取字节数组
     *
     * @param inputStream
     * @return
     * @throws IOException
     */
    public static byte[] readInputStream(InputStream inputStream) throws IOException {
        byte[] buffer = new byte[1024];
        int len = 0;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        while ((len = inputStream.read(buffer)) != -1) {
            bos.write(buffer, 0, len);
        }
        bos.close();
        return bos.toByteArray();
    }


    public static MultipartFile htmlToPdf(String htmlString, String path) {
        try {
            File pdfFile = null;
            com.itextpdf.text.Document document = new com.itextpdf.text.Document(PageSize.A4);
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(path));
            document.open();
            XMLWorkerHelper.getInstance().parseXHtml(writer, document, new ByteArrayInputStream(htmlString.getBytes("UTF-8")), null, Charset.forName("UTF-8"), new AsianFontProvider());
            document.close();
            pdfFile = new File(path);
            if (!pdfFile.exists()) {
                pdfFile.mkdirs();
            }
            FileItem fileItem = new DiskFileItem("mainFile", Files.probeContentType(pdfFile.toPath()), false, pdfFile.getName(), (int) pdfFile.length(), pdfFile.getParentFile());
            try (InputStream input = new FileInputStream(pdfFile);
                 OutputStream os = fileItem.getOutputStream();) {
                IOUtils.copy(input, os);
                return new CommonsMultipartFile(fileItem);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


    static class AsianFontProvider extends XMLWorkerFontProvider {

        @Override
        public Font getFont(final String fontname, final String encoding,
                            final boolean embedded, final float size, final int style,
                            final BaseColor color) {
            BaseFont bf = null;
            try {
                bf = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H",
                        BaseFont.NOT_EMBEDDED);
            } catch (DocumentException | IOException e) {
                e.printStackTrace();
            }
            Font font = new Font(bf, size, style, color);
            font.setColor(color);
            return font;
        }


    }




    /**
     * pdf2img
     * @param pdfFilePath
     * @param imageFilePath
     * @throws Exception
     */
    public static void pdf2image(String pdfFilePath, String imageFilePath) throws Exception {
        //合同pdf2image
        //组装合同文件
        org.icepdf.core.pobjects.Document document = new org.icepdf.core.pobjects.Document();
        document.setFile(pdfFilePath);
        float scale = 2.5f;//缩放比例
        float rotation = 0f;//旋转角度
        //拼装图片
        String[] pics = new String[document.getNumberOfPages()];
        for (int i = 0; i < document.getNumberOfPages(); i++) {
            BufferedImage image = (BufferedImage)
                    document.getPageImage(i, GraphicsRenderingHints.SCREEN, org.icepdf.core.pobjects.Page.BOUNDARY_CROPBOX, rotation, scale);
            RenderedImage rendImage = image;
            try {
                ImageIO.write(rendImage, "jpg", new File(imageFilePath + i));
                pics[i] = imageFilePath + i;
            } catch (IOException e) {
                e.printStackTrace();
            }
            image.flush();
        }
        document.dispose();
        joinImageListVertical(pics, "jpg", imageFilePath);
    }


    /**
     * 纵向拼接一组（多张）图像
     *
     * @param pics    将要拼接的图像数组
     * @param type    写入图像类型
     * @param dst_pic 写入图像路径
     * @return
     */
    public static  boolean joinImageListVertical(String[] pics, String type, String dst_pic) {
        try {
            int len = pics.length;
            if (len < 1) {
                System.out.println("pics len < 1");
                return false;
            }
            File[] src = new File[len];
            BufferedImage[] images = new BufferedImage[len];
            int[][] imageArrays = new int[len][];
            for (int i = 0; i < len; i++) {
                //System.out.println(i);
                src[i] = new File(pics[i]);
                images[i] = ImageIO.read(src[i]);
                int width = images[i].getWidth();
                int height = images[i].getHeight();
                imageArrays[i] = new int[width * height];// 从图片中读取RGB
                imageArrays[i] = images[i].getRGB(0, 0, width, height, imageArrays[i], 0, width);
            }

            int dst_height = 0;
            int dst_width = images[0].getWidth();
            for (int i = 0; i < images.length; i++) {
                dst_width = dst_width > images[i].getWidth() ? dst_width : images[i].getWidth();
                dst_height += images[i].getHeight();
            }
            //System.out.println(dst_width);
            //System.out.println(dst_height);
            if (dst_height < 1) {
                System.out.println("dst_height < 1");
                return false;
            }
            /*
             * 生成新图片
             */
            BufferedImage ImageNew = new BufferedImage(dst_width, dst_height, BufferedImage.TYPE_INT_RGB);
            int height_i = 0;
            for (int i = 0; i < images.length; i++) {
                ImageNew.setRGB(0, height_i, dst_width, images[i].getHeight(), imageArrays[i], 0, dst_width);
                height_i += images[i].getHeight();
            }
            File outFile = new File(dst_pic);
            ImageIO.write(ImageNew, type, outFile);// 写图片
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }


    /**
     * @param path
     * @param htmlStr
     * @throws Exception
     */
    public static File generatePDF(String htmlStr,String path) throws Exception {

        File outputPDFFile = new File(path);
        com.itextpdf.text.Document document = new com.itextpdf.text.Document();
        FileOutputStream fos = new FileOutputStream(outputPDFFile);
        PdfWriter writer = PdfWriter.getInstance(document, fos);

        document.open();
        InputStream htmlInput = new ByteArrayInputStream(htmlStr.getBytes());

        // 使用我们的字体提供器，并将其设置为unicode字体样式
        MyFontsProvider fontProvider = new MyFontsProvider();
        fontProvider.addFontSubstitute("lowagie", "garamond");
        fontProvider.setUseUnicode(true);
        CssAppliers cssAppliers = new CssAppliersImpl(fontProvider);
        HtmlPipelineContext htmlContext = new HtmlPipelineContext(cssAppliers);
        htmlContext.setTagFactory(Tags.getHtmlTagProcessorFactory());
        XMLWorkerHelper.getInstance().getDefaultCssResolver(true);

        XMLWorkerHelper.getInstance().parseXHtml(writer, document, htmlInput, null, Charset.forName("UTF-8"),
                fontProvider);
        document.close();
        writer.close();
        return outputPDFFile;
    }

    /**
     * 重写 字符设置方法，解决中文乱码问题
     */
    public static class MyFontsProvider extends XMLWorkerFontProvider {
        @Override
        public Font getFont(final String fontname, final String encoding,
                            final boolean embedded, final float size, final int style,
                            final BaseColor color) {
            BaseFont bf = null;
            try {
                bf = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Font font = new Font(bf, size, style, color);
            font.setColor(color);
            return font;
        }
    }

}
