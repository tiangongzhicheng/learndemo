package com.tiangongzhicheng.learndemo.utils;

import com.google.common.base.Joiner;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.List;
import java.util.StringJoiner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class StringUtils extends org.apache.commons.lang3.StringUtils {

    /**
     * 将一个数组使用指定分隔符拼接成一个字符串
     * 可以指定前缀和后缀
     */
    public static String stringJoiner(){
        String[] names = {"Bob", "Alice", "Grace"};
        StringJoiner sj = new StringJoiner(", ", "Hello ", "!");
        for (String name : names) {
            sj.add(name);
        }
        System.out.println(sj.toString());
        return sj.toString();
    }


    /**
     * 将{number}用0补充到{length}位数
     * @param number
     * @param length
     * @return
     */
    public static String fillDigitLength(long number, int length){
        String format = "%1$0" + length +"d";
        String result = String.format(format, number);
        return result;
    }

    public static String fillStringLength(String number, String fill, int length){
        StringBuilder stringBuilder = new StringBuilder(number);
        while (stringBuilder.length() <length ){
            stringBuilder.append(org.apache.commons.lang3.StringUtils.isBlank(fill)?" ":fill);
        }
        return stringBuilder.toString();
    }

    /**
     * 金额数字展示两位小数，加千位切分符
     *
     * @param num 金额 单位为分
     * @return java.lang.String
     * @Author wanghui
     * @Date 2019/6/5 11:33
     **/
    static final DecimalFormat DF = new DecimalFormat("#,##0.00");

    public static String formatMoney(BigDecimal amount) {
        if (null == amount) {
            return "";
        }
        return DF.format(amount.doubleValue());
    }

    public static String formatMoney(String num) {
        if (isBlank(num)) {
            return "";
        }
        return DF.format(Double.valueOf(num));
    }

    public static Long yuanToFine(String yuan) {
        return (long) (Double.valueOf(yuan) * 10000);
    }

    private final static Pattern PATTERN = Pattern.compile("[.][0-9]$");

    /**
     * Java 去除小数点后面多余的0
     *
     * @param s
     * @return
     */
    public static String rvZeroAndDot(String s) {
        if (s.isEmpty()) {
            return s;
        }
        if (s.indexOf(".") > 0) {
            s = s.replaceAll("0+?$", "");
            Matcher matcher = PATTERN.matcher(s);
            if (matcher.find()) {
                s = s.replaceAll("[.][0-9]$", matcher.group(0) + "0");
            }
            if (Pattern.matches("(.*)+\\.", s)) {
                s += "00";
            }
            return s.substring(0, s.indexOf(".") + 3);
        } else {
            s += ".00";
        }
        return s;
    }


    /**
     * @Description 秒转换时分秒 HH:mm:ss
     * @date: 2019/6/11 3:31 PM
     */
    public static String formatDate(Integer num) {
        return formatDate(Long.valueOf(num));
    }

    /**
     * @Description 秒转换时分秒 HH:mm:ss
     * @date: 2019/6/11 3:31 PM
     */
    public static String formatDate(Long num) {
        Long hour = num / 3600;
        String hourStr = hour >= 10 ? String.valueOf(hour) : "0" + hour;
        Long min = num / 60 % 60;
        String minStr = min >= 10 ? String.valueOf(min) : "0" + min;
        Long second = num % 60;
        String secondStr = second >= 10 ? String.valueOf(second) : "0" + second;
        return "" + hourStr + ":" + minStr + ":" + secondStr;
    }

    /**
     * 驼峰转下划线
     *
     * @param str
     * @return
     */
    private static final Pattern humpPattern = Pattern.compile("[A-Z]");

    public static String toLine(String str) {
        Matcher matcher = humpPattern.matcher(str);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(sb, "_" + matcher.group(0).toLowerCase());
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    /**
     * 下划线转驼峰
     *
     * @param line
     * @param smallCamel 大小驼峰,是否为小驼峰
     * @return
     */
    private static final Pattern pattern = Pattern.compile("([A-Za-z\\d]+)(_)?");

    public static String toCamlCase(String line, boolean smallCamel) {
        if (line == null || "".equals(line)) {
            return "";
        }
        StringBuffer sb = new StringBuffer();
        Matcher matcher = pattern.matcher(line);
        while (matcher.find()) {
            String word = matcher.group();
            sb.append(smallCamel && matcher.start() == 0 ? Character.toLowerCase(word.charAt(0)) : Character.toUpperCase(word.charAt(0)));
            int index = word.lastIndexOf('_');
            if (index > 0) {
                sb.append(word.substring(1, index).toLowerCase());
            } else {
                sb.append(word.substring(1).toLowerCase());
            }
        }
        return sb.toString();
    }


    public static <T> String applyIn(List<T> list) {
        return "(" + Joiner.on(",").skipNulls().join(list) + ")";
    }
    public static String applyInStr(List<String> list) {
        return "('" + Joiner.on(",").skipNulls().join(list) + "')";
    }

    public static String applyInStr2(List<String> list) {
        return "('" + Joiner.on("','").skipNulls().join(list) + "')";
    }

    //手机
    private static final Pattern MobilePattern = Pattern.compile("^0?(1[3-9])\\d{9}$");
    //固话
    private static final Pattern PhonePattern = Pattern.compile("^(0\\d{2,3})?([2-9]\\d{6,7})+(\\d{1,6})?$");

    public static boolean matchMobile(String mobilePhone) {
        if (isBlank(mobilePhone)) {
            return false;
        }
        Matcher matcher = MobilePattern.matcher(mobilePhone);
        return matcher.matches();
    }

    public static boolean matchPhone(String phone) {
        if (isBlank(phone)) {
            return false;
        }
        Matcher matcher = PhonePattern.matcher(phone);
        return matcher.matches();
    }
}

