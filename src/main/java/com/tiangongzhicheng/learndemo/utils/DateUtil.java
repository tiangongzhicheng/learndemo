package com.tiangongzhicheng.learndemo.utils;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;

/**
 *
 */
public class DateUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(DateUtil.class);

    public static final String yyyy_MM_dd = "yyyy-MM-dd";

    public static final String yyyy_MM_dd_slash = "yyyy/MM/dd";

    public static final String yyyyMMdd = "yyyyMMdd";

    public static final String yyyy_MM_dd_HH_mm_ss = "yyyy-MM-dd HH:mm:ss";

    public static final String yyyy_MM_dd_plus_HH_mm_ss = "yyyy/MM/dd+HH:mm:ss";

    public static final String yyyy_MM_dd_blank_HH_mm_ss = "yyyy/MM/dd HH:mm:ss";


    /**
     * 1天的毫秒数
     */
    public static final Long MILLISECOND_ONE_DAY = 1000 * 60 * 60 * 24L;

    /**
     * 1小时的毫秒数
     */
    public static final Long MILLISECOND_ONE_HOUR = 1000 * 60 * 60L;

    /**
     * 1分钟的毫秒数
     */
    public static final Long MILLISECOND_ONE_MINUTE = 1000 * 60L;

    /**
     * 用指定的格式解析日期
     */
    public static Date parse(String date, String format) {
        DateTimeFormatter dateTimeFormat = DateTimeFormat.forPattern(format);
        return dateTimeFormat.parseDateTime(date).toDate();
    }

    /**
     *yyyy-MM-dd HH:mm:ss
     */
    public static Date parseAll(String date) {
        DateTimeFormatter dateTimeFormat = DateTimeFormat.forPattern(yyyy_MM_dd_HH_mm_ss);
        return dateTimeFormat.parseDateTime(date).toDate();
    }

    /**
     * yyyy-MM-dd
     */
    public static Date parseYYMMDD(String date) {
        DateTimeFormatter dateTimeFormat = DateTimeFormat.forPattern(yyyy_MM_dd);
        return dateTimeFormat.parseDateTime(date).toDate();
    }

    /**
     * 用指定的格式转成日期字符串
     */
    public static String format(Date date, String format) {
        if (date == null) {
            return "";
        }
        DateTime dateTime = new DateTime(date);
        return dateTime.toString(format);
    }

    public static String formatYYYY_MM_DD(Date date) {
        if (date == null) {
            return "";
        }
        DateTime dateTime = new DateTime(date);
        return dateTime.toString(yyyy_MM_dd);
    }
    public static String formatAll(Date date) {
        if (date == null) {
            return "";
        }
        DateTime dateTime = new DateTime(date);
        return dateTime.toString(yyyy_MM_dd_HH_mm_ss);
    }


    /**
     * 转仅保留指定格式的日期。如取当前时间的 yyyy_MM_dd 日期:
     */
    public static Date formatDate(Date date, String format) {
        return parse(format(date, format), format);
    }

    /**
     * 按年增加日期
     */
    public static Date addDateByYear(Date date, int year) {
        DateTime dateTime = new DateTime(date);
        return dateTime.plusYears(year).toDate();
    }

    /**
     * 按月增加日期
     */
    public static Date addDateByMonth(Date date, int month) {
        DateTime dateTime = new DateTime(date);
        return dateTime.plusMonths(month).toDate();
    }

    /**
     * 按天数增加日期
     */
    public static Date addDateByDay(Date date, int days) {
        DateTime dateTime = new DateTime(date);
        return dateTime.plusDays(days).toDate();
    }


    /**
     * 按月份增加日期
     */
    public static Date addDateByMonths(Date date, int months) {
        DateTime dateTime = new DateTime(date);
        return dateTime.plusMonths(months).toDate();
    }

    /**
     * 按小时增加日期
     */
    public static Date addDateByHour(Date date, int hours) {
        DateTime dateTime = new DateTime(date);
        return dateTime.plusHours(hours).toDate();
    }

    /**
     * 取得两个日期间的天数
     */
    public static int getDaysOfTwoDate(Date startDate, Date endDate) {
        // 时间格式统一
        DateTime startDateTime = new DateTime(startDate);
        DateTime endDateTime = new DateTime(endDate);
        startDateTime = startDateTime.withTime(0, 0, 0, 0);
        endDateTime = endDateTime.withTime(0, 0, 0, 0);
        return Math.abs(Days.daysBetween(startDateTime, endDateTime).getDays());
    }

    /**
     * 获取年的第一天
     */
    public static Date getFirstDayOfYear(Date date) {
        DateTime dateTime = new DateTime(date);
        return dateTime.dayOfYear().withMinimumValue().toDate();
    }

    /**
     * 获取年的最后一天
     */
    public static Date getLastDayOfYear(Date date) {
        DateTime dateTime = new DateTime(date);
        return dateTime.dayOfYear().withMaximumValue().toDate();
    }

    /**
     * 获取月的第一天
     */
    public static Date getFirstDayOfMonth(Date date) {
        DateTime dateTime = new DateTime(date);
        return dateTime.dayOfMonth().withMinimumValue().toDate();
    }

    public static Date getFirstDayOfMonth(String date) {
        DateTime dateTime = new DateTime(date);
        return dateTime.dayOfMonth().withMinimumValue().toDate();
    }

    /**
     * 获取月的最后一天
     */
    public static Date getLastDayOfMonth(Date date) {
        DateTime dateTime = new DateTime(date);
        return dateTime.dayOfMonth().withMaximumValue().toDate();
    }

    public static Date getLastDayOfMonth(String date) {
        DateTime dateTime = new DateTime(date);
        return dateTime.dayOfMonth().withMaximumValue().toDate();
    }

    /**
     * 获取当天0点
     */
    public static Date getToday0() {
        return DateTime.now().withTime(0, 0, 0, 0).toDate();
    }

    public static String startPreOneDay() {
        java.time.format.DateTimeFormatter formatter = java.time.format.DateTimeFormatter.ofPattern("yyyy-MM-dd 00:00:00");
        return formatter.format(LocalDateTime.now().minus(1, ChronoUnit.DAYS)).toString();//获取一天之前的开始时间
    }

    public static String endPreOneDay() {
        java.time.format.DateTimeFormatter formatter = java.time.format.DateTimeFormatter.ofPattern("yyyy-MM-dd 23:59:59");
        return formatter.format(LocalDateTime.now().minus(1, ChronoUnit.DAYS)).toString();//获取一天之前的开始时间
    }
    /**
     * 获取当天24点
     */
    public static Date getToday24() {
        return DateTime.now().withTime(23, 59, 59, 999).toDate();
    }

    /*
     * 获取昨天0点
     */
    public static Date getYesterDay0() {
        return addDateByDay(getToday0(), -1);
    }

    public static Date getYesterDay24() {
        return addDateByDay(getToday24(), -1);
    }

    /**
     * 设置时分
     */
    public static Date resetTime(Date date, int hour, int minutes) {
        DateTime dateTime = new DateTime(date.getTime());
        dateTime = dateTime.withTime(hour, minutes, 0, 0);
        return dateTime.toDate();
    }

    public static Date getAppointDayStartTime(String time) {
        return DateTime.parse(time).withTime(00, 00, 00, 000).toDate();
    }

    public static Date getAppointDayEndTime(String time) {
        return DateTime.parse(time).withTime(23, 59, 59, 999).toDate();
    }

    /**
     * 对 2018/09/08，2018-09-08，20180908三种格式的转化
     * @param date
     * @return
     */
    public static Date strToDate(String date) throws ParseException {
        if(date == null){
            return null;
        }
        String yyyy_MM_dd_REGEXP1 = "^[1-9]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$";
        String yyyy_MM_dd_REGEXP2 = "^[1-9]\\d{3}/(0[1-9]|1[0-2])/(0[1-9]|[1-2][0-9]|3[0-1])$";
        String yyyy_MM_dd_REGEXP3 = "^[1-9]\\d{3}(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])$";
        if(date.matches(yyyy_MM_dd_REGEXP1)){
            return new SimpleDateFormat(yyyy_MM_dd).parse(date);
        }else if(date.matches(yyyy_MM_dd_REGEXP2)) {
            return new SimpleDateFormat(yyyy_MM_dd_slash).parse(date);
        }else if(date.matches(yyyy_MM_dd_REGEXP3)) {
            return new SimpleDateFormat(yyyyMMdd).parse(date);
        }
        return null;
    }

    public static Date reduceDateByDay(Date date, Integer days) {
        DateTime dateTime = new DateTime(date);
        return dateTime.minusDays(days).toDate();
    }
}
