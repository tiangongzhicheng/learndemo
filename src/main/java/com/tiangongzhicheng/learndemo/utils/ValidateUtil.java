package com.tiangongzhicheng.learndemo.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



public class ValidateUtil {

    /**
     * 按照标准日期格式校验： 2018/09/08或2018-09-08或20180908三种
     * @param date
     * @return
     */
    public static boolean isValidStandardDate (String date) {
        String yyyy_MM_dd_REGEXP1 = "^[1-9]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$";
        String yyyy_MM_dd_REGEXP2 = "^[1-9]\\d{3}/(0[1-9]|1[0-2])/(0[1-9]|[1-2][0-9]|3[0-1])$";
        String yyyy_MM_dd_REGEXP3 = "^[1-9]\\d{3}(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])$";
        if(!date.matches(yyyy_MM_dd_REGEXP1) && !date.matches(yyyy_MM_dd_REGEXP2) && !date.matches(yyyy_MM_dd_REGEXP3)){
            return false;
        }
        return true;
    }

    /**
     * 指定日期格式校验
     * @param date
     * @return
     */
    public static boolean isValidDate (String date) {
        try {
            new SimpleDateFormat("yyyyMMdd").parse(date);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    /**
     * 返回指定日期格式
     */
    public static String dateFormat(String date) {
        try {
            String formatDate = new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("yyyyMMdd").parse(date));
            return formatDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
    /**
     * 手机号校验
     */
    public static boolean isValidPhone(String phone) {
        //final String  regex = "^((13[0-9])|(14[5|7])|(15([0-3]|[5-9]))|(17[013678])|(18[0-9]))\\d{8}$";
        final String  regex2 = "(\\+?86+/?)?((13[0-9])|(14[5|7])|(15([0-3]|[5-9]))|(16[0-9])|(17[0135678])|(18[0-9])|(19[0-9]))\\d{8}$";
        Pattern p = Pattern.compile(regex2);
        Matcher m = p.matcher(phone);
        return m.matches();
    }
    /**
     * 校验字符是否为整数
     */
    public static boolean isValidNumber (String number) {
        final String numRegex = "[0-9]*";
        Pattern pattern = Pattern.compile(numRegex);
        Matcher m = pattern.matcher(number);
        return m.matches();
    }
    /**
     * 校验字符是否为两位小数，不是就增加或截取为两位小数
     */
    public static boolean isValidTwoDecimals (String number) {
        final String numRegex = "^[0-9]+(.[0-9]{0,2})?$";
        Pattern pattern = Pattern.compile(numRegex);
        Matcher m = pattern.matcher(number);
        return m.matches();
    }
    /**
     * 邮箱校验
     */
    public static boolean isValidEmail(String email) {
        final String pattern1 = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
        Pattern pattern = Pattern.compile(pattern1);
        Matcher m = pattern.matcher(email);
        return m.matches();
    }

    /**
     * 邮编校验
     * @param telNum
     * @return
     */
    public static boolean isZipCode(String telNum){
        String regex = "[0-9]\\d{5}(?!\\d)";
        Pattern p = Pattern.compile(regex,Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(telNum);
        return m.matches();
    }

    /**
     * 校验银行卡号
     * @param bankNo
     * @return
     */
    public static boolean isValidBankNo(String bankNo) {
        final String  regex = "^\\d{15,19}$";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(bankNo);
        return m.matches();
    }

    public static boolean isFixedPhone(String fixedPhone){
        String reg="(?:(\\(\\+?86\\))(0[0-9]{2,3}\\-?)?([2-9][0-9]{6,7})+(\\-[0-9]{1,4})?)|(?:(86-?)?(0[0-9]{2,3}\\-?)?([2-9][0-9]{6,7})+(\\-[0-9]{1,4})?)";
        String reg2="(?:(\\(\\+?86\\))(0[0-9]{2,3}+(/|-)?)?([2-9][0-9]{6,7})+((-|/)+[0-9]{1,4})?)|(?:(86-?)?(0[0-9]{2,3}+(/|-)?)?([2-9][0-9]{6,7})+((-|/)+[0-9]{1,4})?)";
        return Pattern.matches(reg2, fixedPhone);
    }

    public static boolean isValidMoney(String money){
        String reg="^\\d{1,9}(\\.\\d{1,2})?$";
        return Pattern.matches(reg, money);
    }

}
