package com.tiangongzhicheng.learndemo.utils;

import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.metadata.BaseRowModel;
import com.alibaba.excel.metadata.Sheet;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.tiangongzhicheng.learndemo.exception.ExcelException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;


@Component
@Slf4j
public class ExcelOperationUtil {


    /**
     * 读取指定的excel内容
     * @param file     excel文件
     * @param classType  excel对应的实体
     * @param sheetNo   读第几个excel
     * @param headLineNum   从第几行读起（0为第一行）
     * @return
     * @throws IOException
     */
    public static <T>List<T> readExcel(MultipartFile file, Class<? extends BaseRowModel> classType, int sheetNo, int headLineNum) throws IOException {
        ExcelListener excelListener = new ExcelListener();
        ExcelReader reader = getReader( file, excelListener);
        if (reader == null) {
            return null;
        }
        Sheet sheet = new Sheet(sheetNo, headLineNum, classType==null?null:classType);
        reader.read(sheet);
        return (List<T>)excelListener.getDatas();
    }

    public static List<List<String>> readExcel(File file, BaseRowModel rowModel, int sheetNo, int headLineNum) throws IOException {
        ExcelListenerString excelListener = new ExcelListenerString();
        String filename = file.getName();
        if (filename == null || (!filename.toLowerCase().endsWith(".xls") && !filename.toLowerCase().endsWith(".xlsx"))) {
            throw new ExcelException("文件格式错误！");
        }
        FileInputStream fileInputStream = new FileInputStream(file);
        ExcelReader reader = new ExcelReader(fileInputStream, null, excelListener, false);
        if (reader == null) {
            return null;
        }
        Sheet sheet = new Sheet(sheetNo, headLineNum, rowModel==null?null:rowModel.getClass());
        reader.read(sheet);
        return excelListener.getDatas();
    }

    public static Object readExcel(InputStream inputStream,  Class<? extends BaseRowModel> classType, int sheetNo, int headLineNum) throws IOException {
        ExcelListener excelListener = new ExcelListener();
        ExcelReader reader = new ExcelReader(inputStream, null, excelListener, false);
        if (reader == null) {
            return null;
        }
        Sheet sheet = new Sheet(sheetNo, headLineNum, classType==null?null:classType);
        reader.read(sheet);
        return excelListener.getDatas();
    }

    private static ExcelReader getReader(MultipartFile file,
                                         ExcelListener excelListener) {
        String filename = file.getOriginalFilename();
        if (filename == null || (!filename.toLowerCase().endsWith(".xls") && !filename.toLowerCase().endsWith(".xlsx"))) {
            throw new ExcelException("文件格式错误！");
        }
        InputStream inputStream;
        try {
            inputStream = new BufferedInputStream(file.getInputStream());
            return new ExcelReader(inputStream,null, excelListener, false);
        } catch (IOException e) {
            log.info("错误信息===={}",e);
            e.printStackTrace();
        }
        return null;
    }

    public static void writeExcel(HttpServletResponse response, List<List<String>> list, String fileName, String sheetName){
        ExcelWriter writer = new ExcelWriter(getOutputStream(fileName, response), ExcelTypeEnum.XLSX);
        Sheet sheet = new Sheet(1, 0, null);
        sheet.setSheetName(sheetName);
        writer.write0(list, sheet);
        writer.finish();
    }

    /**
     * 导出文件时为Writer生成OutputStream
     */
    private static OutputStream getOutputStream(String fileName, HttpServletResponse response) {
        //创建本地文件
        String filePath = fileName + ".xlsx";
        File dbfFile = new File(filePath);
        try {
            if (!dbfFile.exists() || dbfFile.isDirectory()) {
                dbfFile.createNewFile();
            }
            fileName = new String(filePath.getBytes(), "ISO-8859-1");
            response.addHeader("Content-Disposition", "filename=" + fileName);
            return response.getOutputStream();
        } catch (IOException e) {
            throw new ExcelException("创建文件失败！");
        }
    }

    /**
     * 获取excel的字符串，用空格对齐长度
     * @throws IOException
     */
    public static void getStringFromExcel(String path) throws IOException {

        File file = new File(path);
        List<List<String>> list = readExcel(file, null, 1, 0);
        list.forEach(a ->{
            a.forEach(b ->
                System.out.print(StringUtils.fillStringLength(b,null,20))
            );
            System.out.println();
        });
    }

    public static void main(String[] args) throws IOException {

    }


}
