package com.tiangongzhicheng.learndemo.utils;

import com.tiangongzhicheng.learndemo.pojo.FileList;
import com.tiangongzhicheng.learndemo.pojo.SendEMailReq;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.core.io.UrlResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.util.CollectionUtils;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.UUID;

public class EMailUtils {


    private String FROM_USER;

    private String PASSWORD;

    private String path;

    @Autowired
    private JavaMailSenderImpl javaMailSender;


    /**
     * 发送文本email
     * @param param
     * @throws Exception
     */
    public void sendTextMail(SendEMailReq param) throws Exception{
        if(StringUtils.isNotBlank(param.getFrom()) && StringUtils.isNotBlank(param.getPassword())) {
            javaMailSender.setUsername(param.getFrom());
            javaMailSender.setPassword(param.getPassword());
        }else {
            javaMailSender.setUsername(FROM_USER);
            javaMailSender.setPassword(PASSWORD);
        }
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(StringUtils.isNotBlank(param.getFrom()) && StringUtils.isNotBlank(param.getPassword())?param.getFrom():FROM_USER);
        message.setTo(param.getTo());
        message.setSubject(param.getSubject());
        message.setText(param.getContent());
        if (!CollectionUtils.isEmpty(param.getCc())) {
            String[] cc = new String[param.getCc().size()];
            message.setCc(param.getCc().toArray(cc));
        }
        javaMailSender.send(message);
    }


    public void sendHtmlMail(SendEMailReq param) throws MessagingException {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();

        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
        mimeMessageHelper.setFrom(FROM_USER);
        mimeMessageHelper.setTo(param.getTo());
        mimeMessageHelper.setSubject(param.getSubject());
        mimeMessageHelper.setText(param.getContent(), true);
        if (!CollectionUtils.isEmpty(param.getCc())) {
            String[] cc = new String[param.getCc().size()];
            mimeMessageHelper.setCc(param.getCc().toArray(cc));
        }

        javaMailSender.send(mimeMessage);
    }


    /**
     * 发送附件email
     * @param toUser
     * @param subject
     * @param content
     * @param filePath
     * @param fileName
     * @throws MessagingException
     * @throws IOException
     */
    public void sendAttachmentMail(String toUser, String subject, String content, String filePath, String fileName) throws MessagingException, IOException {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();

        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
        mimeMessageHelper.setFrom(FROM_USER);
        mimeMessageHelper.setTo(toUser);
        mimeMessageHelper.setSubject(subject);
        mimeMessageHelper.setText(content, true);

        URL url = new URL(filePath);
        InputStreamSource inputStreamSource = new InputStreamSource() {
            @Override
            public InputStream getInputStream() throws IOException {
                return url.openStream();
            }
        };
        mimeMessageHelper.addAttachment(fileName, inputStreamSource);

        javaMailSender.send(mimeMessage);
    }

    /**
     * 发送附件email
     * @param param
     * @throws MessagingException
     * @throws IOException
     */
    public void sendAttachmentMail(SendEMailReq param) throws MessagingException, IOException {
        if(StringUtils.isNotBlank(param.getFrom()) && StringUtils.isNotBlank(param.getPassword())) {
            javaMailSender.setUsername(param.getFrom());
            javaMailSender.setPassword(param.getPassword());
        }else {
            javaMailSender.setUsername(FROM_USER);
            javaMailSender.setPassword(PASSWORD);
        }
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();

        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true,"utf-8");
        mimeMessageHelper.setFrom(StringUtils.isNotBlank(param.getFrom()) && StringUtils.isNotBlank(param.getPassword())?param.getFrom():FROM_USER);
        mimeMessageHelper.setTo(param.getTo());
        mimeMessageHelper.setSubject(param.getSubject());
        mimeMessageHelper.setText(param.getContent(), true);
        for (FileList fileList : param.getFileLists()) {
            URL url = new URL(fileList.getFileUrl());
            InputStreamSource inputStreamSource = new InputStreamSource() {
                @Override
                public InputStream getInputStream() throws IOException {
                    return url.openStream();
                }
            };
            mimeMessageHelper.addAttachment(fileList.getFileName(), inputStreamSource);
        }
        if (!CollectionUtils.isEmpty(param.getCc())) {
            String[] cc = new String[param.getCc().size()];
            mimeMessageHelper.setCc(param.getCc().toArray(cc));
        }
        javaMailSender.send(mimeMessage);
    }

    /**
     * 发送图片email
     * @param toUser
     * @param subject
     * @param content
     * @param imgId
     * @param imgPath
     * @throws MessagingException
     */
    public void sendImgMail(String toUser, String subject, String content, String imgId, String imgPath) throws MessagingException {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();

        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
        mimeMessageHelper.setFrom(FROM_USER);
        mimeMessageHelper.setTo(toUser);
        mimeMessageHelper.setSubject(subject);
        mimeMessageHelper.setText(content, true);

        FileSystemResource img = new FileSystemResource(new File(imgPath));
        mimeMessageHelper.addInline(imgId, img);

        javaMailSender.send(mimeMessage);
    }


    public void sendTemplateMail(String toUser, String subject, String content) throws MessagingException {
        SendEMailReq param = new SendEMailReq();
        param.setTo(toUser);
        param.setSubject(subject);
        param.setContent(content);
        this.sendHtmlMail(param);
    }

    /**
     * 发送图片email
     * @param param
     * @throws MessagingException
     * @throws MalformedURLException
     */
    public void sendImgMail(SendEMailReq param) throws MessagingException, MalformedURLException {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
        mimeMessageHelper.setFrom(FROM_USER);
        mimeMessageHelper.setTo(param.getTo());
        mimeMessageHelper.setSubject(param.getSubject());

        StringBuffer start = new StringBuffer("<html><body>");

        for (FileList fileList : param.getFileLists()) {
            UrlResource urlResource = new UrlResource(fileList.getFileUrl());
            String uuid = UUID.randomUUID().toString();
            System.out.println(uuid);
            start.append("<img src=\'cid:" + uuid + "\'/>");
            mimeMessageHelper.addInline(uuid, urlResource);

        }
        System.out.println(String.valueOf(start.append("</body></html>")));
        mimeMessageHelper.setText(String.valueOf(start.append("</body></html>")), true);

        if (!CollectionUtils.isEmpty(param.getCc())) {
            String[] cc = new String[param.getCc().size()];
            mimeMessageHelper.setCc(param.getCc().toArray(cc));
        }
        javaMailSender.send(mimeMessage);
    }
}
