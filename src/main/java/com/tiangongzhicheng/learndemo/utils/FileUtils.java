package com.tiangongzhicheng.learndemo.utils;

import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ResourceLoader;

import javax.annotation.Resource;
import javax.annotation.Resources;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.ArrayList;
import java.util.Spliterator;
import java.util.zip.ZipOutputStream;

@Configuration
public class FileUtils {

    @Autowired
    private HttpClientUtil httpClientUtil;
    @Resource
    private ResourceLoader resourceLoader;

    /**
     * 根据https地址从远端下载文件
     * @param originalFilename
     * @param url
     * @param response
     * @throws IOException
     */
    public void downloadFile(String originalFilename, String url, HttpServletResponse response)throws IOException {
        ByteArrayOutputStream outputStream = httpClientUtil.sendHttpGetStream(url);
        byte[] bytes = outputStream.toByteArray();
        outputStream.flush();
        outputStream.close();
        response.setContentType("application/octet-stream");
        String filename = java.net.URLEncoder.encode( originalFilename, "UTF-8");
        response.addHeader("Content-disposition", "attachment;filename*=utf-8'zh_cn'" + filename );
        OutputStream toClient = new BufferedOutputStream(response.getOutputStream());
        toClient.write(bytes, 0, bytes.length);
        toClient.flush();
        toClient.close();
    }

    /**
     * 根据https地址从远端下载文件
     * @param originalFilename
     * @param url
     * @throws IOException
     */
    public void downloadFile(String originalFilename, String url)throws IOException {
        ByteArrayOutputStream outputStream = httpClientUtil.sendHttpGetStream(url);
        byte[] bytes = outputStream.toByteArray();
        outputStream.flush();
        outputStream.close();
        String filename = java.net.URLEncoder.encode( originalFilename, "UTF-8");
        File saveDir = new File("D:/test33/");
        if (!saveDir.exists()) {
            saveDir.mkdir();
        }
        FileOutputStream fileOutputStream = new FileOutputStream(saveDir+"/132.jpg");
        fileOutputStream.write(bytes);
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    /**
     * 从静态资源下载文件
     * @param fileName
     * @param path
     * @param response
     */
    public void downloadInsertExcelTemplate(String fileName,String path, HttpServletResponse response) {
        InputStream inputStream = null;
        ServletOutputStream servletOutputStream = null;
        try {
            org.springframework.core.io.Resource resource = resourceLoader.getResource("classpath:"+path);
            inputStream = resource.getInputStream();
            response.addHeader("Content-Disposition", "attachment;filename=" + new String(fileName.getBytes("UTF-8"), "iso-8859-1"));
            servletOutputStream = response.getOutputStream();
            IOUtils.copy(inputStream, servletOutputStream);
            response.flushBuffer();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (servletOutputStream != null) {
                    servletOutputStream.close();
                }
                if (inputStream != null) {
                    inputStream.close();
                }
                // 召唤jvm的垃圾回收器
                System.gc();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 读取CSV文件
     * @param filepath
     */
    public void readCSVFile(String filepath){
        //String filepath = "C:\\Users\\1\\Desktop\\新建文本文档.csv";
        File csv = new File(filepath); // CSV文件路径
        csv.setReadable(true);//设置可读‪‪‪
        csv.setWritable(true);//设置可写‪‪‪‪
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(csv));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        String line = "";
        String everyLine = "";
        ArrayList<String> allString = new ArrayList<>();
        try {
            while ((line = br.readLine()) != null) // 读取到的内容给line变量
            {
                everyLine = line;
                System.out.println(everyLine);
                allString.add(everyLine);
            }
            System.out.println("csv表格中所有行数：" + allString.size());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//    public void batchDownloadFile( HttpServletResponse response) {
//
//        ByteArrayOutputStream bos = new ByteArrayOutputStream();
//        ZipOutputStream zos = new ZipOutputStream(bos);
//        UrlFilesToZipUtilis s = new UrlFilesToZipUtilis();
//
//    }

}
